import csv
import datetime

output_file = 'inserts_to_bill.txt'

mg_file = 'utf_mg.csv'

mn_file = 'utf_mn.csv'
#TODO Tests on mn_file, maybe change column order

values = []


def get_current_unixtime():
    """
    There is a special field for direction insertion date in UTM DB. Don't remember right now, should it be filled
    automatically or it just taken from request.
    :return: unix timestamp, seconds only.
    """
    date = datetime.datetime.now().timestamp()
    return str(int(date))


def get_mg_mask_quantifier(base_len: int, utm_mask: str):
    """
    Make quantifier for the end of UTM regexp, after mask already parsed.
    :param base_len: int
    :param utm_mask: str
    :return: {x,y}, {x}
    """
    quantifier_symbols = '[0-9]'
    number_length = 11
    if utm_mask == '':
        quantifier_count = '{' + str(number_length - base_len) + '}'
    elif '(' in utm_mask:
        length = []
        for mask in utm_mask.split('(')[1].split(')')[0].split('|'):
            if '[' in mask:
                length.append(str(number_length - base_len - mask.split('[')[0].__len__() - 1))
            else:
                length.append(str(number_length - base_len - mask.__len__()))
        min_quantifier_count = str(min(length))
        max_quantifier_count = str(max(length))
        quantifier_count = '{' + min_quantifier_count + ',' + max_quantifier_count + '}'
    else:
            if '[' in utm_mask:
                quantifier_count = '{' + str(number_length - base_len - utm_mask.split('[')[0].__len__() - 1) + '}'
            else:
                quantifier_count = '{' + str(number_length - base_len - utm_mask.__len__()) + '}'

    return quantifier_symbols + quantifier_count


def parse_mask_with_hyphen(mask_with_hyphen: str):
    """
    Mask with hyphen like 56-57 should be parsed separately for correct change.
    :param mask_with_hyphen: str. 56-57
    :return: str. 5[6-7]
    """
    parsed_mask_with_hyphen = ''
    if mask_with_hyphen.__len__() > 3:
        draft_parts = mask_with_hyphen.split('-')
        for i in range(0, draft_parts.__len__()):
            if draft_parts[0][i] == draft_parts[1][i]:
                parsed_mask_with_hyphen = parsed_mask_with_hyphen + draft_parts[0][i]
            else:
                parsed_mask_with_hyphen = parsed_mask_with_hyphen + \
                                    '[' + draft_parts[0][i] + '-' + draft_parts[1][i] + ']'
        return str(parsed_mask_with_hyphen)
    else:
        return str(mask_with_hyphen)


def get_mask(masks_column: str):
    """
    Parsing column with numbers mask from PSI.
    :param masks_column: str
    :return: string with regexp for utm CdPN mask
    """
    masks = masks_column.split(';')
    utm_mask = ''
    utm_regexp = ''
    for i in range(0, masks.__len__()):
        if masks[i] == '':
            masks.pop(i)
    for mask_column in masks:
        base = mask_column.split(':')[0]
        if ' ' in base:
            base = base.split(' ')[1]
        if ':' in mask_column:
            multimask = mask_column.split(':')[1]
            if ',' in multimask:
                utm_mask = '('
                mask_parts = multimask.split(',')
                for mask in mask_parts:
                    if '-' in mask:
                        if utm_mask == '(':
                            utm_mask = utm_mask + parse_mask_with_hyphen(mask)
                        else:
                            utm_mask = utm_mask + '|' + parse_mask_with_hyphen(mask)
                    else:
                        if utm_mask == '(':
                            utm_mask = utm_mask + mask
                        else:
                            utm_mask = utm_mask + '|' + mask
                utm_mask = utm_mask + ')'
            else:
                if '-' in multimask:
                    utm_mask = '[' + parse_mask_with_hyphen(multimask) + ']'
                else:
                    utm_mask = utm_mask + multimask

        if utm_regexp == '':
            utm_regexp = '^' + base + utm_mask + get_mg_mask_quantifier(base.__len__(), utm_mask)
        else:
            utm_regexp = utm_regexp + '|^' + base + utm_mask + get_mg_mask_quantifier(base.__len__(), utm_mask)

    return utm_regexp


def set_value_mg(utm_direction_name: str, utm_phone_mask: str):
    """
    Make another value string for UTM insert request with correct data.
    :param utm_direction_name:
    :param utm_phone_mask:
    :return:
    """
    values.append("('" + utm_direction_name + "','" + get_current_unixtime() + ",'" + utm_phone_mask + "',0,1,0,0,2),\n")


###############
## Logic starts
###############

start_line = "insert into 'tel_directions_v2' (`name`,`create_date`,`called_prefix`,`calling_prefix_regexp`,`called_prefix_regexp`,`skip`,`is_deleted`,`dir_type`) Values \n"

#read file into var, remove first line
with open(mg_file, newline='') as csvfile:
    content_csv = csv.reader(csvfile, delimiter=';', quotechar='"')
    content = [row for row in content_csv]
content.pop(0)
#print(content)

#make first line for request file (and file itself, if necessary)
with open(output_file, 'w+') as output:
    output.write(start_line)

#set values for request
for line in content:
#    print(line)
    set_value_mg(str(line[0]), get_mask(line[1]))

#write values to output file
with open(output_file, 'a') as output:
    for value in values:
        output.write(value)

#TODO End last line with ';' instead of ','
